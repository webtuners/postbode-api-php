<?php
/*
 * Postbode PHP API Client
 *
 * @package    Postbode.nu
 * @author     Mark Hameetman <mark@bureaupartners.nl>
 * @copyright  (c) 2014 Postbode B.V.
 * @version    2.0
 * @link       https://bitbucket.org/postbodenu/postbode-api-php
 */

class Postbode {
	
	private $sEmailaddress;
	private $sPassword;
	private $sAPI					=	'https://www.postbode.nu/api';
	private $aPostData				=	array();
	private $sResponseType			=	'json';
	private $aAllowedReponseTypes	=	array('json', 'xml');
	private $sResponseCode;
	private $aAvailablePostTypes	=	array('standaard', 'premium', 'aangetekend', 'internationaal');
	private $iEnvelopeID;
	private $sPostType;
	
	const	NO_POSTTYPE				=	'Er is geen Postbode::sPostType ingevuld.';
	const	NO_ENVELOPEID			=	'Er is geen Postbode::iEnvelopeID ingevuld.';
	
	public function __construct($sEmailaddress, $sPassword){
		$this->sEmailaddress		=	$sEmailaddress;
		$this->sPassword			=	$sPassword;
	}
	
	public function setResponseType($sType){
		$sType						=	strtolower($sType);
		if(in_array($sType, $this->aAllowedResponseTypes)){
			$this->sResponseType	=	$sType;
		}else{
			$this->sResponseType	=	'json';
		}
	}

	public function getResponseCode(){
		return $this->sResponseCode;
	}
	
	public function setEnvelopeID($iEnvelopeID){
		$this->iEnvelopeID 				=	$iEnvelopeID;
	}
	
	public function getEnvelopeID(){
		return $this->iEnvelopeID;
	}
	
	public function setPostType($sPostType){
		$sPostType					=	strtolower($sPostType);
		if(in_array($sPostType, $this->aAvailablePostTypes)){
			$this->sPostType		=	$sPostType;
		}else{
			$this->sPostType		=	NULL;
		}
	}
	
	public function getPostType(){
		return $this->sPostType;
	}
	
	/*
	 *		Brief toevoegen aan concepten in de Postbode.nu interface
	 */
	public function addLetter($sFilename, $sName = NULL){
		if($sName == NULL){
			$sName = $this->__extractName($sFilename);
		}
		$this->__setPostData('name', $sName);
		$this->__setPostData('document', $this->__encodeFile($sFilename));
		return $this->sendRequest('postkantoor', 'POST');
	}
	
	/*
	 *		Brief direct verzenden via Postbode.nu
	 */
	public function sendLetter($sFilename, $sName = NULL){
		if($sName == NULL){
			$sName = $this->__extractName($sFilename);
		}
		$this->__setPostData('name', $sName);
		$this->__setPostData('document', $this->__encodeFile($sFilename));
		if(in_array($this->getPostType(), $this->aAvailablePostTypes)){
			$this->__setPostData('type', $this->getPostType());
		}else{
			die(Postbode::NO_POSTTYPE);
		}
		// Bij keuze Standaard is alleen EnvelopeID 1 toegestaan.
		if($this->getPostType() == 'standaard'){
			$this->setEnvelopeID(1);
		}
		
		if(is_numeric($this->getEnvelopeID())){
			$this->__setPostData('envelope', $this->getEnvelopeID());
		}else{
			die(Postbode::NO_ENVELOPEID);
		}
		return $this->sendRequest('verzend', 'POST');
	}
	
	private function sendRequest($sMethod, $sType = 'GET'){
		$oConnection				=	curl_init();
		curl_setopt($oConnection, CURLOPT_URL, $this->$sAPI.'/'.$sMethod.'.'.$this->sResponseType);
		curl_setopt($oConnection, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($oConnection, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($oConnection, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($oConnection, CURLOPT_USERPWD, $this->sEmailaddress.':'.$this->sPassword);
		curl_setopt($oConnection, CURLOPT_USERAGENT, 'Postbode.nu PHP API Client 2.0');
		if($sType == 'POST'){
			curl_setopt($oConnection, CURLOPT_POST, 1);
			curl_setopt($oConnection, CURLOPT_POSTFIELDS, $this->aPostData);
		}
		curl_setopt($oConnection,	CURLOPT_RETURNTRANSFER,	true);
		$sResponse					=	curl_exec($oConnection);
		$this->sResponseCode 		=	curl_getinfo($oConnection, CURLINFO_HTTP_CODE); 
		curl_close($oConnection);
		return $sResponse;
	}

	private function __setPostData($sName, $aData){
		$this->aPostData[$sName]	=	$aData;
	}
	
	private function __encodeArray($aData){
		return base64_encode(serialize($aData));
	}

	private function __encodeFile($sFilename){
		return base64_encode(file_get_contents($sFilename));
	}
	
	private function __extractName($sFilename){
		return end(explode('/', $sFilename));
	}
	
}


?>